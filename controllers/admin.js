const Product = require('../models/product');

exports.getAddProduct = (req, res, next) => {
    res.render('admin/add-product', {
        pageTitle: 'Add Product',
        path: '/admin/add-product',
        formsCSS: true,
        productCSS: true,
        activeAddProduct: true });
};

exports.postAddProduct = (req,res, next ) => { //moze tak samo sie nazywac jesli roznia sie metody, get i post
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const price = req.body.price;
    const description = req.body.description;
    const product = new Product(title, imageUrl, price, description);
    product.save();
    res.redirect('/');
};

exports.getProducts = (req, res, next) => { //middleware function , controller
    Product.fetchAll(products =>{
        res.render('admin/products', { //view
            prods: products,
            pageTitle: 'Admin Products',
            path:'/admin/products'
        });
    });
};

exports.getIndex = (req,res,next) => {
    Product.fetchAll(products =>{
        res.render('shop/index', { //view
            prods: products,
            pageTitle: 'Shop',
            path:'/'
        });
    });
};

exports.getCart = (req,res,next) => {
    res.render('shop/cart', { //view
        pageTitle: 'Your Cart',
        path:'/cart'
    });
};

exports.getCheckout = (req,res,next) => {
    res.render('shop/checkout', { //view
        pageTitle: 'Checkout',
        path:'/checkout'
    });
};


