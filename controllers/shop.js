const Product = require('../models/product');

exports.getProducts = (req, res, next) => { //middleware function , controller
    Product.fetchAll(products =>{
        res.render('shop/product-list', { //view
            prods: products,
            pageTitle: 'All Products',
            path:'/products'
        });
    });
};

exports.getIndex = (req,res,next) => {
     Product.fetchAll(products =>{
        res.render('shop/index', { //view
            prods: products,
            pageTitle: 'Shop',
            path:'/'
        });
    });
};

exports.getCart = (req,res,next) => {
    res.render('shop/cart', { //view
        pageTitle: 'Your Cart',
        path:'/cart'
    });
};
exports.getOrders = (req,res,next) => {
    res.render('shop/orders', { //view
        pageTitle: 'Your Orders',
        path:'/orders'
    });
};

exports.getCheckout = (req,res,next) => {
    res.render('shop/checkout', { //view
        pageTitle: 'Checkout',
        path:'/checkout'
    });
};
